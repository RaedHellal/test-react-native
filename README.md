# test react native
 React Native test app 

## Presentation

This project was created with react-native-cli.

The data used in this project is from: https://jsonplaceholder.typicode.com/posts. 

The app present two screens:

 . Home: This screen just present a button to navigate to posts.

 . Posts: This screen contain the data fetched from jsonplaceholder.

## Table of Contents

* [Available Scripts](#available-scripts)
  * [npm start](#npm-start)
  * [npm test](#npm-test)
  * [npm run ios](#npm-run-ios)
  * [npm run android](#npm-run-android)
  * [npm run generate-apk](#npm-run-generate-apk)
  * [npm run install-apk](#npm-run-install-apk)
* [Dependencies](#dependencies)
  * [Navigation](#Navigation)
  * [Global State management](#Global-State-management)
  * [axios](#axios)

## Available Scripts

Before running the project you need to do `npm install` to install differents dependencies and then:

### `npm start`

Runs your app in development mode.

start react native js bundler.

#### `npm test`

Runs the [jest](https://github.com/facebook/jest) test runner on your tests.

#### `npm run ios`

Like `npm start`, but also attempts to open your app in the iOS Simulator if you're on a Mac and have it installed.

#### `npm run android`

Like `npm start`, but also attempts to open your app on a connected Android device or emulator. 

#### `npm run generate-apk`

This command generate a release apk.

#### `npm run install-apk`

This command install the release apk on the adnroid emulator or connected android device.

## Dependencies
List of dependencies and their usage 

### Navigation:

 Usage of a stack navigator for the navigation in the app, and to implement this we nedd to install different dependencies for react navigation:
`@react-native-community/masked-view`
`@react-navigation/native`
`@react-navigation/stack`
`react-native-gesture-handler`
`react-native-reanimated`
`react-native-safe-area-context`
`react-native-screens`

### Global State management:

For the global state management, usage of redux and its diferent dependencies:
`@react-native-async-storage/async-storage`: to implement local storage
`redux-persist`: to store state in local storage
`redux-thunk`: we have an asynchronous call with our action fetch posts
`react-redux`
`redux`

### axios 

There is an instance of axios created in utils to do differents api calls.