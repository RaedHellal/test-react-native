import { initialState } from './state';

export default function postsReducer(state = initialState, action) {
  switch (action.type) {
    case "set_posts":
      return {
        ...state,
        posts: action.posts
      }
    default:
      return state;
  }
}