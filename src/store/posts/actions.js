import {sendAsyncRequest} from "../../utils/axios";

export const fetchPosts =  () => async (dispatch) => {
    let config = {
        url:'posts',
        method:'get'
    }
    try{
        let response = await sendAsyncRequest(config)
        let posts = response.data.slice(0,50).sort((first, second)=> first.title.localeCompare(second.title))
        dispatch({
            type:'set_posts',
            posts: posts
        })
    } catch(error){
        console.log(error)
    }
}