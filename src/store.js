import { persistStore, persistCombineReducers } from "redux-persist";
import { applyMiddleware, createStore, compose } from "redux";
import AsyncStorage from "@react-native-async-storage/async-storage";
import reducers from './store/index'
import thunk from 'redux-thunk'

const middlewares = [thunk];

const config = {
    key: 'root',
    storage: AsyncStorage,
    blacklist: [],
    whitelist: ['postsReducer']
};

const reducer = persistCombineReducers(config, reducers);

let store;

export default function configureStore() {
    store = createStore(reducer, undefined, compose(
        applyMiddleware(...middlewares)
    ));

    let persistor = persistStore(store);

    return { persistor, store };
}
