import React from 'react';
import {View, TouchableOpacity, Text} from 'react-native';

export const Home = (props) => {
    return (
      <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
         <TouchableOpacity style={styles.button}
            onPress={() => props.navigation.navigate('Posts')}>
            <Text style={styles.text}> Go to Posts</Text>
          </TouchableOpacity>
      </View>
    )
  }

  const styles = {
    text:{
        fontSize:20,
        textAlign:'center',
        fontWeight:'bold',
        color:'white'
    },
    button:{
        width:'75%',
        borderRadius:15,
        padding:5,
        backgroundColor:'blue',
    }
  }