import React, { useState, useEffect } from 'react';
import { View, FlatList, Text, Keyboard, TouchableWithoutFeedback, TextInput, Image } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { fetchPosts } from "../store/posts/actions";
import { Post } from '../components/Post';
import { Spinner } from '../components/common/Spinner';

export const Posts = () => {
    let [posts, setPosts] = useState([]);
    let [loading, setLoading] = useState(false);
    let [page, setPage] = useState(1);
    let [search, setSearch] = useState('');
    let [lastPage, setLastPage] = useState(1)
    let fetchedPosts = useSelector(state => state.postsReducer.posts);
    let dispatch = useDispatch();

    if (fetchedPosts.length === 0) {
        dispatch(fetchPosts())
        if (!loading) {
            setLoading(true)
        }
    }

    const renderItem = ({ item }) => (
        <Post title={item.title} description={item.body} />
    );

    useEffect(() => {
        if (fetchedPosts.length > 0) {
            setPosts(fetchedPosts.slice(0, 10))
            setLastPage(fetchedPosts.length % 10 === 0 ? fetchedPosts.length / 10 : Math.floor(fetchedPosts.length / 10) + 1)
        }
    }, [fetchedPosts]);

    useEffect(() => {
        if (loading) {
            setTimeout(() => {
                setLoading(false)
            }, 500)
        }
    }, [loading]);



    const refreshPosts = () => {
        setLoading(true)
        let result = fetchedPosts.filter(post => post.title.includes(search))
        setPosts(result.slice(0, 10))
        setLastPage(result.length % 10 === 0 ? result.length / 10 : Math.floor(result.length / 10) + 1)
        setPage(1)
    }

    const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
        return layoutMeasurement.height + contentOffset.y >= contentSize.height - 20;
    }

    const callNextPage = () => {
        let nextPage = page + 1;
        if (page < lastPage && loading === false) {
            setLoading(true)
            let result = fetchedPosts.filter(post => post.title.includes(search))
            setPosts(result.slice(0, 10 * nextPage))
            setPage(nextPage)
        }
    };

    const clearSearch = () => {
        setSearch('');
        Keyboard.dismiss();
        setLoading(true)
        setPosts(fetchedPosts.slice(0, 10))
        setLastPage(fetchedPosts.length % 10 === 0 ? fetchedPosts.length / 10 : Math.floor(fetchedPosts.length / 10) + 1)
        setPage(1)
    }
    useEffect(()=>{
        if(search.length> 0){
            refreshPosts()
        }
    },[search])
    return (
        <View style={{ flex: 1 }}>
            <View style={styles.containerSearch}>
                <TouchableWithoutFeedback onPress={()=>{refreshPosts();Keyboard.dismiss();}}>
                    <Image source={require('../assets/rechercher.png')} />
                </TouchableWithoutFeedback>
                <TextInput style={styles.input}
                    placeholder='Search'
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={search}
                    autoCorrect={false}
                    returnKeyType={"done"}
                    onChangeText={(text) => {
                        setSearch(text.trim())
                    }}
                    onBlur={() => {
                        if (search.length < 3) {
                            refreshPosts();
                            Keyboard.dismiss();
                        }
                    }}
                    onSubmitEditing={() => {
                        refreshPosts();
                        Keyboard.dismiss();
                    }}
                />
                {search.trim() !== '' && <TouchableWithoutFeedback onPress={() => { clearSearch() }}>
                    <Image source={require('../assets/clean.png')} />
                </TouchableWithoutFeedback>}
            </View>
            {loading && <Spinner />}
            {posts.length > 0 ? <FlatList
                data={posts}
                renderItem={renderItem}
                keyExtractor={item => 'p' + item.id}
                onScroll={({ nativeEvent }) => {
                    if (isCloseToBottom(nativeEvent)) {
                        callNextPage();
                    }
                }}
                ListFooterComponent={()=>{
                    return (page === lastPage && !loading && <Text style={styles.footer}>No more posts</Text>)
                }}
            /> : !loading && <Text style={styles.footer}>No items found</Text>}
        </View>
    )
}

const styles = {
    text: {
        fontSize: 26
    },
    footer: {
        width: '100%',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 16,
        padding:10
    },
    input: {
        marginHorizontal: 15,
        width: '70%',
        height: 35,
        color: '#82838a',
        fontSize: 17.5,
        padding: 0
    },
    containerSearch: {
        height: 51,
        backgroundColor: 'white',
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderColor: '#d9dadc'
    }
}