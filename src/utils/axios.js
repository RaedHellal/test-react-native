import axios from 'axios';

export const sendAsyncRequest = axios.create({
    timeout: 2000,
    baseURL: "https://jsonplaceholder.typicode.com"
});