import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import {Home} from './screens/Home';
import {Posts} from './screens/Posts';


const StackPatient = createStackNavigator();

AppStack = () => {
  return (
    <StackPatient.Navigator>
      <StackPatient.Screen name="Home" component={Home} />
      <StackPatient.Screen name="Posts" component={Posts} />
    </StackPatient.Navigator>
  );
}

export default AppStack;
