import React from 'react';
import { View, Text } from 'react-native';
import {capitalize} from '../utils/utils';

export const Post = (props) => {
    let {title, description} = props
    return (
        <View style={styles.postContainer}>
            <Text style={styles.title}> {capitalize(title)} </Text>
            <Text style={styles.description}> {description} </Text>
        </View>
    )
}

const styles = {
    postContainer:{ 
        flex: 1,
        backgroundColor:'white',
        padding:10,
        margin:7,
        alignItems:'flex-start',
        justifyContent:'flex-start',
        borderRadius:5,
        borderWidth: 1,
        borderColor: '#d9dadc'
    },
    title:{
        fontSize:18,
        fontWeight: "bold"
    },
    description:{
        fontSize:14
    }
}