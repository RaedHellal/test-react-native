import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

const Header = (props) => {
    let { onBackPress, withBack, title } = props;
    return (
        <View style={styles.header}>
            <View style={styles.back}>
                {
                    withBack &&
                    <TouchableOpacity hitSlop={{ top: 25, bottom: 25, left: 25, right: 25 }} onPress={() => { onBackPress() }}>
                        <Text>Back</Text>
                    </TouchableOpacity>
                }
            </View>
            <Text style={styles.textHeader}>
                {title}
            </Text>
            <View style={styles.back}>

            </View>
        </View>
    )
}

const styles = {
    textHeader: {
        fontSize: 18.5,
        color: '#403f45',
        width: '50%',
        textAlign: 'center',
    },
    header: {
        zIndex: 1,
        height: 48,
        width: '100%',
        backgroundColor:'white',
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15
    },
    back: {
        width: "22%",
        height: 25,
        alignItems: 'flex-start',
        justifyContent: 'center',
    }
};

export { Header }
