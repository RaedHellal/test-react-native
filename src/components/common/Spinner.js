import React from 'react';
import { ActivityIndicator, View } from 'react-native';

export const Spinner = () => {
  return(<View style={{zIndex:22}}>
    <ActivityIndicator size="large" color="#2d96de" />
  </View>)
}
