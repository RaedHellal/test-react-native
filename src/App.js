import 'react-native-gesture-handler';
import React from 'react';
import {
  SafeAreaView,
} from 'react-native';
import Router from './Router';
import { NavigationContainer } from '@react-navigation/native';
import { Provider } from 'react-redux';
import configureStore from './store';
import { PersistGate } from 'redux-persist/es/integration/react';

const { persistor, store } = configureStore();


const App = () => {
  return (
    <Provider store={store}>
      <PersistGate
        persistor={persistor}>
        <NavigationContainer>
          <SafeAreaView style={{ flex: 1 }}>
            <Router />
          </SafeAreaView>
        </NavigationContainer>
      </PersistGate>
    </Provider>
  );
};

export default App;
